DevilutionX port for PlayStation Classic

==Installation==

BleemSync/Eris: Copy 'devilutionx' launcher folder to the appropriate launchers folder, e.g.  
* bleemsync/etc/bleemsync/SUP/launchers/  
* project_eris/etc/project_eris/SUP/launchers/  

Project Eris: Use .mod package from https://classicmodscloud.com/project_eris/mods/  

Rename your DIABDAT.MPQ to all-lowercase (diabdat.mpq) and place it in the launcher/app folder.  

If you have issues, check that your diabdat.mpq file's MD5 checksum matches one of the following:  
* 68f049866b44688a7af65ba766bef75a (Original US)  
* 011bc6518e6166206231080a4440b373 (EU/MAC/GOG)  

RetroBoot has a similar mod package.

As of v0.8.5, AutoBleem does not suspend/close its UI when launching applications from EvoUI, thus the UI remains running in the background. This ties up the console's Alsa audio device preventing DevX from launching properly. If this changes in the future, it should be possible to create an EvoUI App launcher (run.sh) for AutoBleem without using RetroBoot.

==Credits and Thanks==  
* AJenbo for the massive, quick and helpful support, workaround for Wayland mouse issues and for pointing out an SDL audio bug that had me tugging my hair.  
* Screemer for the toolchain used to build the binary and library dependencies. If not for the readily available pre-built toolchain I would have never dabbled in crosscompiling.  
* MMC community for the suggestion, motivation, and friendly nagging.  

The minor tweaks needed to get this working (wayland-specific mouse cursor fix, SDL audio resampling in Storm FMV playback routine) are in the commit history in the src folder. All credit goes to the DevilutionX team for their wonderful work in SDL-ifying the reverse-engineered Diablo source making it trivial to build against any platform supporting SDL2. It did not take much work to get things working!